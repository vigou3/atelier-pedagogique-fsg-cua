%%% Copyright (C) 2019 Vincent Goulet
%%%
%%% Ce fichier et tous les fichiers mentionnés dans les
%%% commandes \include et \input ci-dessous font partie du projet
%%% «Innovations pédagogiques et conception universelle de
%%% l'apprentissage: rapport du terrain»
%%% https://gitlab.com/vigou3/atelier-pedagogique-fsg-cua
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage[round]{natbib}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{fontawesome5}              % icônes
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %% =============================
  %%  Informations de publication
  %% =============================
  \title{Innovations pédagogiques et conception universelle de
    l'apprentissage: rapport du terrain}
  \author{Vincent Goulet}
  \renewcommand{\year}{2019}
  \renewcommand{\month}{12}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/atelier-pedagogique-fsg-cua}

  %% =======================
  %%  Apparence du document
  %% =======================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Mathématiques en arev et ajustement de la taille des autres
  %% polices Fira; https://tex.stackexchange.com/a/405211/24355
  \usepackage{arevmath}
  \setsansfont[%
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}]{Fira Sans Book}

  %% Couleurs additionnelles
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % bandeau rouge UL
  \definecolor{or}{rgb}{1,0.8,0}            % bandeau or UL
  \colorlet{alert}{mLightBrown} % alias pour couleur Metropolis
  \colorlet{dark}{mDarkTeal}    % alias pour couleur Metropolis
  \colorlet{code}{mLightGreen}  % alias pour couleur Metropolis

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Introduction à la science des données},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Bibliographie
  \renewcommand{\newblock}{}    % https://tex.stackexchange.com/a/1971/24355
  \renewcommand{\bibsection}{}  % laisser tomber \section
  \bibliographystyle{francais}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Lien externe
  \newcommand{\link}[2]{\href{#1}{#2~\faExternalLink*}}

  %% Renvoi vers GitLab sur la page de copyright
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Raccourcis usuels vg
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}     \newlength{\imagewidth}
  \newlength{\logoheight}

\begin{document}

%% frontmatter
\input{couverture-avant}
\input{notices}

%% mainmatter
\section{Enseigner à l'ère numérique}

\begin{frame}
  \centering
  Des séances intensives de prise de notes\dots \\
  \begin{minipage}{0.55\linewidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/796px-Monkcopyistwoodcut} \\
    \tiny Domaine public, \href{https://commons.wikimedia.org/wiki/File:Monkcopyistwoodcut.jpg}{Wikimedia Commons}
  \end{minipage}
\end{frame}

\begin{frame}
  \centering
  \dots\ au Syndrome PowerPoint \\
  \begin{minipage}{\linewidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/Satterfield_watches_Congress_be_boring} \\
    \tiny Domaine public, \href{https://commons.wikimedia.org/wiki/File:Satterfield_watches_Congress_be_boring.jpg}{Wikimedia Commons}
  \end{minipage}
\end{frame}

\begin{frame}
  \begin{center}
    \textbf{Fin des années 2000}
  \end{center}
  \begin{center}
    La salle de classe est obsolète! \\
    Le futur est dans le \emph{e-learning}!
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \textbf{Retour du balancier}
  \end{center}
  \begin{center}
    Les étudiants aiment rencontrer leur enseignant \\
    \pause
    \dots\ en autant que ça en vaut la peine.
  \end{center}
\end{frame}

\section{Enseignement hybride}

\begin{frame}
  \frametitle{Définition (une parmi plusieurs)}

  \begin{quote}
    Fusion bien réfléchie d'activités en classe et à distance [\dots]
    intégrées de telle sorte que les \alert{forces de chacunes}
    contribuent à l'atteinte des objectifs d'apprentissage en fonction
    du contexte.

    \citep[traduction très libre]{Garrison:blended:2007}
  \end{quote}
\end{frame}

\begin{frame}
  \frametitle{Modèles d'enseignement hybride}

  \begin{description}[Enriched-virtual model]
  \item[\emph{Rotation model}] Alternance entre des activités à
    distance et des périodes d'enseignement en présentiel
  \item[\emph{Flex model}] Étudiants principalement à distance, mais
    sous la supervision synchrone d'un enseignant
  \item[\emph{Self-blending model}] Notre approche \emph{comodale}:
    étudiants choisissent entre suivre un cours à distance ou en
    classe avec l'enseignant
  \item[\alert<2>{\emph{Enriched-virtual model}}] Apprentissages à
    distance enrichis périodiquement par des activités en classe
  \end{description}

  {\footnotesize \citep{Stalker:blended:2012,Friesen:blended:2012}}
\end{frame}

\begin{frame}
  \frametitle{Motivation}

  \begin{itemize}
  \item \alert{Pas} pour améliorer l'accessibilité aux cours
  \item Cours qui «roule» en partie seul
  \item Améliorer la portée des travaux pratiques
    \begin{itemize}
    \item rétroaction
    \item charge de travail
    \end{itemize}
  \end{itemize}
\end{frame}

%% These are defined outside the following frame to avoid troubles
%% with overlays.
\setlength{\unitlength}{1mm}
\newsavebox{\activity}
\savebox{\activity}(4,0){\color{alert}\circle*{4}}
\newsavebox{\week}
\savebox{\week}(12,0){\color{dark}\rule[-1mm]{12mm}{2mm}}

\begin{frame}
  \frametitle{Mon modèle d'enseignement hybride}
  \centering
  \begin{picture}(125,60)
    \put(0,40){\usebox{\activity}}
    \put(4,40){\usebox{\week}}
    \put(18,40){\usebox{\week}} \put(30,40){\usebox{\activity}}
    \put(34,40){\usebox{\week}} \put(46,40){\usebox{\activity}}
    \put(50,40){\usebox{\week}}
    \put(64,40){\usebox{\week}} \put(70,44){\makebox(0,0){\color{alert}\faPlusSquare}}
    \put(78,40){\usebox{\week}} \put(90,40){\usebox{\activity}}
    \put(94,40){\usebox{\week}}
    \multiput(109,40)(2.5,0){3}{\circle*{0.7}}

    \only<2->{
      \put(0,37.5){\line(0,-1){31}}
      \put(1,30){%
        \begin{minipage}[t]{35mm}
          \faUsers \\ Activité de lancement
          \begin{itemize}
          \item bienvenue
          \item plan de cours
          \item premiers pas
          \end{itemize}
        \end{minipage}}
    }

    \only<3->{
      \put(40,37.5){\line(0,-1){31}}
      \put(41,30){%
        \begin{minipage}[t]{50mm}
          \faUser \\ Étude à distance
          \begin{itemize}
          \item théorie et exemples
          \item exercises
          \item évaluations formatives
          \end{itemize}
        \end{minipage}}
    }

    \only<4->{
      \put(90,37.5){\line(0,-1){31}}
      \put(91,30){%
        \begin{minipage}[t]{45mm}
          \faUsers \\ Ateliers en classe
          \begin{itemize}
          \item révision
          \item activités de transfert
          \item sprints de codage
          \end{itemize}
        \end{minipage}}
    }

    \only<5->{
      \put(70,46.2){\line(0,1){12}}
      \put(71,50){%
        \parbox[b]{40mm}{\faShare* \\
          \href{https://youtu.be/14c5N8CfnUo}{Vidéos explicatives~\faYoutube}}}
    }
  \end{picture}
\end{frame}

\begin{frame}
  \frametitle{Conditions de succès}

  \begin{itemize}[<+->]
  \item Faire confiance aux étudiants
  \item Ne pas faire confiance aux étudiants \texttt{:-)}
  \end{itemize}
\end{frame}

\section{Documents de référence numériques}

\begin{frame}
  \frametitle{Pierres d'assises de mes cours hybrides}
  \setlength{\fboxsep}{0pt}
  \centering

  \begin{tabularx}{0.75\linewidth}{cXcXc}
    \fbox{\includegraphics[height=0.4\textheight,keepaspectratio]{images/programmer-avec-r}}
    && \fbox{\includegraphics[height=0.4\textheight,keepaspectratio]{images/theorie-credibilite-avec-r}}
    && \fbox{\includegraphics[height=0.4\textheight,keepaspectratio]{images/modelisation-distributions-sinistres-avec-r}}
    \\
    \raisebox{1ex}{\footnotesize\link{https://vigou3.gitlab.io/programmer-avec-r}{site
    web}} \\[1ex]
    \fbox{\includegraphics[height=0.4\textheight,keepaspectratio]{images/methodes-numeriques-en-actuariat_simulation}}
    && \fbox{\includegraphics[height=0.4\textheight,keepaspectratio]{images/methodes-numeriques-en-actuariat_analyse-numerique}}
    && \fbox{\fbox{\includegraphics[height=0.4\textheight,keepaspectratio]{images/methodes-numeriques-en-actuariat_algebre-lineaire}}}
  \end{tabularx}
\end{frame}

\section{Conception universelle \\ de l'apprentissage}

\begin{frame}[plain]
  \frametitle{Approche pédagogique inclusive}

  \begin{minipage}{0.3\linewidth}
    \centering
    Égalité \\
    \medskip
    \includegraphics[width=\linewidth,keepaspectratio]{images/equality.png}
  \end{minipage}
  \hfill
  \pause
  \begin{minipage}{0.3\linewidth}
    \centering
    Équité \\
    \medskip
    \includegraphics[width=\linewidth,keepaspectratio]{images/equity.png}
  \end{minipage}
  \hfill
  \pause
  \begin{minipage}{0.3\linewidth}
    \centering
    Conception universelle \\
    \medskip
    \includegraphics[width=\linewidth,keepaspectratio]{images/udl.png}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Deux initiatives porteuses}

  \begin{enumerate}
  \item Travail longitudinal
    \bigskip
    \begin{center}
      \begin{picture}(125,10)
        \thicklines
        \put( 0.0,0){\framebox(42.5,10){\faUser~conception}}
        \put(42.5,5){\vector(1,0){7.5}}
        \put(50.0,0){\framebox(25,10){\faUsers~prototype}}
        \put(75.0,5){\vector(1,0){7.5}}
        \put(82.5,0){\framebox(42.5,10){\faUser~livraison}}
      \end{picture}
    \end{center}
    \bigskip
    \pause
  \item «Mon travail pratique en 180 secondes»
  \end{enumerate}
\end{frame}

\section{Moins bons coups \\ (ou demi-succès)}

\begin{frame}
  \frametitle{Innovations pédagogique laissées de côté}

  \begin{itemize}
  \item Banque de questions de révision avec la plateforme
    StackOverflow
  \item Foire aux questions collaborative sous forme de wiki
  \item Enregistrement des séances
  \item Cartes conceptuelles
  \end{itemize}
\end{frame}

%% backmatter
\begin{frame}
  \frametitle{Bibliographie}

  \nocite{Bates:10fundamentals:2016,Friesen:lecture:2016}

  \bibliography{education}
\end{frame}

\input{colophon}
\input{couverture-arriere}

\end{document}

%%% Local Variables:
%%% mode: noweb
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
