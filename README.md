# Innovations pédagogiques et conception universelle de l'apprentissage: rapport du terrain

Matériel d'une présentation effectuée dans le cadre des activités
d'échange pédagogique de la [Faculté des sciences et de génie](https://fsg.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Résumé

J'enseigne dans une formule hybride bien adaptée aux grands groupes depuis quelques années. Après avoir suivi en 2018-2019 le Programme d’appui au développement d’une approche pédagogique inclusive, j'a intégré à mes cours diverses innovations pédagogiques: approches expérientielle et inclusive, développement des compétences numériques, collaboration, pratique réflexive. Je vous propose un tour d'horizon de mes bons coups... et des idées que j'ai préféré laisser de côté.
